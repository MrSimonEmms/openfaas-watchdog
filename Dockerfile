ARG ARCH
ARG NODE_VERSION=12-alpine
FROM ${ARCH}/alpine AS watchdog
ARG WATCHDOG_VERSION
# "of" or "classic"
ARG WATCHDOG_TYPE
ARG WATCHDOG_CLASSIC_URL="https://github.com/openfaas/faas/releases/download/%version%/fwatchdog%arch%"
ARG WATCHDOG_OF_URL="https://github.com/openfaas-incubator/of-watchdog/releases/download/%version%/of-watchdog%arch%"
WORKDIR /opt/app
RUN echo ${ARCH} \
  && echo ${WATCHDOG_VERSION} \
  && echo ${WATCHDOG_TYPE} \
  && apk add --no-cache dpkg \
  && current_arch=$(dpkg --print-architecture) \
  && parsed_arch="${current_arch/musl-linux/''}" \
  && arch="${parsed_arch/-amd64/''}" \
  && export var_name=$(echo "WATCHDOG_${WATCHDOG_TYPE}_URL" | tr "[:lower:]" "[:upper:]") \
  && export download_url=$(eval echo \$${var_name} | sed -e "s/%version%/${WATCHDOG_VERSION}/" | sed -e "s/%arch%/${arch}/") \
  && echo $download_url \
  && wget ${download_url} -O fwatchdog \
  && chmod +x ./fwatchdog \
  && cp fwatchdog /usr/local/bin
CMD ["fwatchdog"]
